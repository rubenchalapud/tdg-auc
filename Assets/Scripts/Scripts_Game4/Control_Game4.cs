using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Control_Game4 : MonoBehaviour
{
    public TextMeshProUGUI textoTiempo, textoPuntos, textoPuntosWin;
    public GameObject gameOverBox, gameOverWinBox, mensajeInicio;
    public GameObject indicadorTiempo;
    public float timeOver = 15f;
    private int timeToText, puntos;
    public bool juegoEstaActivo, gameWinActivo;
    private string puntosPrefsName="PuntosGame4";
    // Start is called before the first frame update
    void Start()
    {
        juegoEstaActivo = true;
        gameWinActivo = false;
        puntos = 0;
    }

    // Update is called once per frame
    void Update()
    {
        ContadorTiempo();
    }

    public void ContadorTiempo()
    {
        timeOver -= Time.deltaTime;
        timeToText = (int)timeOver;
        textoTiempo.text = "¡" + timeToText.ToString() + " Seg!";
        indicadorTiempo.transform.position = indicadorTiempo.transform.position + new Vector3(36*Time.deltaTime, 0, 0);
        
        if(timeOver <= 0)
        {
            GameOver();
        } 
    }

    public void ActualizarPuntos(int puntosASumar)
    {
        puntos += puntosASumar;
        textoPuntos.text = puntos + "";
        textoPuntosWin.text = puntos + "";
    }

    public void GameOver() 
    {
        if(gameWinActivo == false)
        {
            gameOverBox.gameObject.SetActive(true);
            JuegoFinalizado();
        }
    }

    public void GameWin()
    {
        gameOverWinBox.gameObject.SetActive(true);
        gameWinActivo = true;
        JuegoFinalizado();
    }
    
    public void JuegoFinalizado()
    {
        juegoEstaActivo = false;
        mensajeInicio.gameObject.SetActive(false);
        textoTiempo.gameObject.SetActive(false);
        indicadorTiempo.gameObject.SetActive(false);
        Invoke("ChangeScene", 5f);
    }
    private void ChangeScene()
    {
        SceneManager.LoadScene("Benefits");
    }

    private void OnDestroy() {
        SaveData();
    }

    private void SaveData()
    {
        PlayerPrefs.SetInt(puntosPrefsName , puntos);
        //Para cargar los datos en la otra escena:
        //int puntosUno = PlayerPrefs.GetInt("PuntosGame1", 0);
    }
     
}
