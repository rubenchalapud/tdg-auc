using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drag_Botella : MonoBehaviour
{
    private Rigidbody rbObjetivo;
    private Vector2 posInicio, posFinal, direccion;
    private float touchTimeInicio, touchTimeFinal, tiempoIntervalo;
    private int puntos;
    
    [SerializeField]
    float throwFuerzaEnXY = 1f;

    [SerializeField]
    float throwFuerzaEnZ = 50f;
    private Control_Game4 controlGame4;

    void Start()
    {
        rbObjetivo = GetComponent<Rigidbody>();
        controlGame4 = GameObject.Find("GestoJuego4").GetComponent<Control_Game4>();
    }

    void Update()
    {
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            touchTimeInicio = Time.time;
            posInicio = Input.GetTouch(0).position;
        }

        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            touchTimeFinal = Time.time;
            tiempoIntervalo = touchTimeFinal - touchTimeInicio;
            posFinal = Input.GetTouch(0).position;
            direccion = posInicio - posFinal;
            rbObjetivo.useGravity = true;
            rbObjetivo.isKinematic = false;
            rbObjetivo.AddForce(-direccion.x * throwFuerzaEnXY, - direccion.y * throwFuerzaEnXY, throwFuerzaEnZ / tiempoIntervalo);
        }

    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.tag == "C1")
        {
            puntos = 1000;
        }
        if(other.tag == "C2")
        {
            puntos = 500;
        }
        if(other.tag == "C3")
        {
            puntos = 100;
        }
        if(other)

        controlGame4.ActualizarPuntos(puntos);
        controlGame4.GameWin();
        Destroy(gameObject);
    }

}
