using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;  
using TMPro;

public class DialogImpacts : MonoBehaviour
{
    public GameObject lasthit;
    public Vector3 collision = Vector3.zero;
    public TextMeshProUGUI DialogueText;
    public string[] Sentences;
    private int Index;
    public float DialogueSpeed;
    public GameObject DialogCanvas;
    private bool nextText = true;
    private bool onlyonce1 = true;
    private bool onlyonce2 = false;
    private bool onlyonce3 = false;
    private bool onlyonce4 = false;

   private void Update()
   {
       Ray raymodel;
       RaycastHit hitmodel;

       var ray = new Ray(this.transform.position, this.transform.forward);
       RaycastHit hit;
       if (Physics.Raycast(ray, out hit, 100))
       {
           lasthit = hit.transform.gameObject;
           collision = hit.point;
           if (lasthit == GameObject.Find("PonerModeloAqui") && onlyonce1 == true)
           {
             StartDialog();
             Index = 0;
           }
           if (lasthit == GameObject.Find("PonerModeloAqui2") && onlyonce2 == true)
           {
             StartDialog2();
             nextText = true;
             
           }
           if (lasthit == GameObject.Find("PonerModeloAqui3") && onlyonce3 == true)
           {
             StartDialog3();
             nextText = true;
             
           }
           if (lasthit == GameObject.Find("PonerModeloAqui4") && onlyonce4 == true)
           {
             StartDialog4();
             nextText = true;
             
           }
       }

       raymodel = Camera.main.ScreenPointToRay(Input.mousePosition);
          if (Physics.Raycast(raymodel, out hitmodel) && Input.GetMouseButton(0))
          {
              if (hitmodel.collider.name == "PonerModeloAqui2") 
              {
                  SceneManager.LoadScene("Impacts_salud"); 
              }
              if (hitmodel.collider.name == "PonerModeloAqui4") 
              {
                  SceneManager.LoadScene("Impacts_ambiental"); 
              }
              if (hitmodel.collider.name == "PonerModeloAqui3") 
              {
                  SceneManager.LoadScene("Impacts_social"); 
              }
          }


   }

   public void StartDialog()
    {
    
     if(nextText == true){ 
            NextSentence(); 
            nextText = false;
            onlyonce1 = false;
            onlyonce2 = true;
            onlyonce3 = true;
            onlyonce4 = true;
      }


    }

    public void StartDialog2()
    {
    
     if(nextText == true){ 
            NextSentence2(); 
            nextText = false;
            onlyonce1 = true;
            onlyonce2 = false;
            onlyonce3 = true;
            onlyonce4 = true;
      }


    }

    public void StartDialog3()
    {
    
     if(nextText == true){ 
            NextSentence3(); 
            nextText = false;
            onlyonce1 = true;
            onlyonce2 = true;
            onlyonce3 = false;
            onlyonce4 = true;
      }


    }

    public void StartDialog4()
    {
    
     if(nextText == true){ 
            NextSentence4(); 
            nextText = false;
            onlyonce1 = true;
            onlyonce2 = true;
            onlyonce3 = true;
            onlyonce4 = false;
      }


    }

    void NextSentence()
    {
        if(Index <= Sentences.Length - 1)
        {
            DialogueText.text = "";
            StartCoroutine(WriteSentence());
        }
    }


    IEnumerator WriteSentence()
    {
        foreach(char Character in Sentences[Index].ToCharArray())
            {
                DialogueText.text += Character;
                yield return new WaitForSeconds(DialogueSpeed);   
            }
            nextText = true;
    }

    void NextSentence2()
    {
        if(Index <= Sentences.Length - 1)
        {
            DialogueText.text = "";
            StartCoroutine(WriteSentence2());
        }
    }


    IEnumerator WriteSentence2()
    {
        foreach(char Character in Sentences[1].ToCharArray())
            {
                DialogueText.text += Character;
                yield return new WaitForSeconds(DialogueSpeed);   
            }
            nextText = true;
    }

    void NextSentence3()
    {
        if(Index <= Sentences.Length - 1)
        {
            DialogueText.text = "";
            StartCoroutine(WriteSentence3());
        }
    }


    IEnumerator WriteSentence3()
    {
        foreach(char Character in Sentences[2].ToCharArray())
            {
                DialogueText.text += Character;
                yield return new WaitForSeconds(DialogueSpeed);   
            }
            nextText = true;
    }
   
    void NextSentence4()
    {
        if(Index <= Sentences.Length - 1)
        {
            DialogueText.text = "";
            StartCoroutine(WriteSentence4());
        }
    }


    IEnumerator WriteSentence4()
    {
        foreach(char Character in Sentences[3].ToCharArray())
            {
                DialogueText.text += Character;
                yield return new WaitForSeconds(DialogueSpeed);   
            }
            nextText = true;
    }
}
