using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderAR : MonoBehaviour
{
    public GameObject ARCamera;
    public GameObject MainCamera;
    public GameObject ButtonLeft;
    public GameObject ButtonRight;
    public Slider ARSlider;
    
    public void offAR()
    {
        if(ARSlider.value == 1)
        {    
                ARCamera.SetActive(false);
                MainCamera.SetActive(true);
                ButtonLeft.SetActive(true);
                ButtonRight.SetActive(true);
                MainCamera.transform.position = new Vector3(0f,0f,0f);
                MainCamera.transform.rotation = new Quaternion(0f,0f,0f,0f);

        }
        else
        {
                ARCamera.SetActive(true);
                MainCamera.SetActive(false);
                ButtonLeft.SetActive(false);
                ButtonRight.SetActive(false); 
        }
    }
    
}
