using System.Collections;  
using System.Collections.Generic;  
using UnityEngine;  
using UnityEngine.SceneManagement;  

public class ChangeScene: MonoBehaviour {  

    public void Recycle() {  
        SceneManager.LoadScene("Recycle");  
    }  
    public void Impacts() {  
        SceneManager.LoadScene("Impacts");  
    }
    public void Juego_4() {  
        SceneManager.LoadScene("Juego_4");  
    }
    public void Impacts_ambiental() {  
        SceneManager.LoadScene("Impacts_ambiental");  
    }
    public void Impacts_salud() {  
        SceneManager.LoadScene("Impacts_salud");  
    }   
    public void Impacts_social() {  
        SceneManager.LoadScene("Impacts_social");  
    }        

}