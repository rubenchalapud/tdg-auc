using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScript : MonoBehaviour
{
    public Transform Menu;
    public CanvasGroup background;
    public Transform HelpText;
    bool ShowMenu = false;
   
/*    private void OnEnable()
    {
        background.alpha = 0;
        background.LeanAlpha(0.60f, 0.5f);

        Menu.localPosition = new Vector2(-Screen.width,217);
        Menu.LeanMoveLocalX(178, 0.5f).setEaseOutExpo().delay = 0.1f;

    }
*/
    
    public void CloseMenu()
    {
    if(ShowMenu == true){ 
        background.LeanAlpha(0, 0.5f);

        HelpText.LeanMoveLocalX(-479, 0.5f).setEaseOutExpo().delay= 0.1f;

        Menu.localPosition = new Vector2(178,217);
        Menu.LeanMoveLocalX(539, 0.5f).setEaseOutExpo().delay = 0.1f;
        ShowMenu = false;
        Debug.Log(ShowMenu);
    }
    

    else {
        background.alpha = 0;
        background.LeanAlpha(0.60f, 0.5f);

        HelpText.LeanMoveLocalX(-195,0.5f).setEaseInExpo();
        Menu.LeanMoveLocalX(178, 0.5f).setEaseInExpo();
        ShowMenu = true;
        Debug.Log(ShowMenu);
    }
    }
}
