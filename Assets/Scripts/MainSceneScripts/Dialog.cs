using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class Dialog : MonoBehaviour
{

    public TextMeshProUGUI DialogueText;
    public string[] Sentences;
    private int Index = 0;
    public float DialogueSpeed;
    public GameObject DialogCanvas;
    private bool nextText = true;
    public GraphicRaycaster raycaster;

    void Awake()
     {
         // Get both of the components we need to do this
         this.raycaster = GetComponent<GraphicRaycaster>();
     }

    // Start is called before the first frame update
    void Start()
    {
         
    }

    // Update is called once per frame esto es void update por si falla
    public void Update()
    {
    
     if(nextText == true){
        
        //int IdDialog = DialogCanvas.GetInstanceID(); 
        if(Input.GetKeyDown(KeyCode.Mouse0)){
            
        /*     PointerEventData pointerData = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();
 
             //Raycast using the Graphics Raycaster and mouse click position
            pointerData.position = Input.mousePosition;
            this.raycaster.Raycast(pointerData, results);


            if(results.Count != 0)
            {
             
            } */
            NextSentence(); 
            nextText = false;
        }
       
      }
    }

    void NextSentence()
    {
        if(Index <= Sentences.Length - 1)
        {
            DialogueText.text = "";
            StartCoroutine(WriteSentence());
        }
    }


    IEnumerator WriteSentence()
    {
        foreach(char Character in Sentences[Index].ToCharArray())
            {
                DialogueText.text += Character;
                yield return new WaitForSeconds(DialogueSpeed);   
            }
            Index++;
            nextText = true;
    }
}
