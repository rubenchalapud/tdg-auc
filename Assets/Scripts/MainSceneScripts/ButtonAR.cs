using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public GameObject ARCamera;
    public GameObject MainCamera;
    bool ShowAR = true;

    // Update is called once per frame
    public void offAR()
    {
        if(ShowAR == true)
        {
            if(ARCamera != null && MainCamera != null)
            {
                ARCamera.SetActive(false);
                MainCamera.SetActive(true);
                ShowAR = false;
            }
        }
        else
        {
            if(ARCamera != null && MainCamera != null)
            {
                ARCamera.SetActive(true);
                MainCamera.SetActive(false); 
                ShowAR = true; 
            }
        }

        
    }

   
}
