using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationMainCamera : MonoBehaviour
{
    private int state = 0;
    public GameObject MainCamera;

    
    public void RotateCameraR()
    {
        if(state == 0){
            MainCamera.transform.Rotate(new Vector3(0f,90f,0f));
            state = 1;
        }
        if(state == 1){
            MainCamera.transform.Rotate(new Vector3(0f,90f,0f));
            state = 2;
        }
        if(state == 2){
            MainCamera.transform.Rotate(new Vector3(0f,90f,0f));
            state = 3;
        }
        if(state == 3){
            MainCamera.transform.Rotate(new Vector3(0f,90f,0f));
        }

    }
    public void RotateCameraL()
    {
        if(state == 0){
            MainCamera.transform.Rotate(new Vector3(0f,-90f,0f));
            state = 1;
        }
        if(state == 1){
            MainCamera.transform.Rotate(new Vector3(0f,-90f,0f));
            state = 2;
        }
        if(state == 2){
            MainCamera.transform.Rotate(new Vector3(0f,-90f,0f));
            state = 3;
        }
        if(state == 3){
            MainCamera.transform.Rotate(new Vector3(0f,-90f,0f));
        }

    }
}
