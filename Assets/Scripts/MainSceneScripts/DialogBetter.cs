using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;


public class DialogBetter : MonoBehaviour
{

    public TextMeshProUGUI DialogueText;
    public string[] Sentences;
    private int Index = 0;
    public float DialogueSpeed;
    public GameObject DialogCanvas;
    private bool nextText = true;
    public GameObject Gota1;
    public GameObject Gota2;
    public GameObject Button1;
    public GameObject Button2;
    public GameObject ButtonRecycle;
    public GameObject ButtonLookMore;
    public GameObject Text1;
    public GameObject Text2;
    bool ShowButtons = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void StartDialog()
    {
    
     if(nextText == true){ 
            NextSentence(); 
            nextText = false;
       
      }

      if (Index >= 3) {
        Gota1.SetActive(true);
        Gota2.SetActive(true);
        //Gota1.GetComponent<MeshRenderer>().enabled = true;
        //Gota2.GetComponent<MeshRenderer>().enabled = true;
        }
      
      

    }

    void NextSentence()
    {
        if(Index <= Sentences.Length - 1)
        {
            DialogueText.text = "";
            StartCoroutine(WriteSentence());
        }

        if (Index == 3) {
        Button1.LeanMoveLocalX(-203,0.5f).setEaseInExpo();
        Button2.LeanMoveLocalX(218,0.5f).setEaseInExpo();
        ButtonLookMore.SetActive(false);
        ButtonRecycle.SetActive(true);
        Text1.SetActive(false);
        Text2.SetActive(true);
        }
        
    }


    IEnumerator WriteSentence()
    {
        foreach(char Character in Sentences[Index].ToCharArray())
            {
                DialogueText.text += Character;
                yield return new WaitForSeconds(DialogueSpeed);   
            }
            
            Index++;
            if(Index == Sentences.Length){
                nextText = false;
            }
            else{
            nextText = true;
            }
    }

    
}

