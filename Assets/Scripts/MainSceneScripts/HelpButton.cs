using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpButton : MonoBehaviour
{
    public GameObject HelpText;
    bool ShowMenu = false;

    public void Help() {  
    if(ShowMenu == true){ 
        HelpText.LeanMoveLocalX(-479, 0.5f).setEaseOutExpo().delay= 0.1f;
        ShowMenu = false;
    }
    

    else {
        HelpText.LeanMoveLocalX(-195,0.5f).setEaseInExpo();
        ShowMenu = true;
    }
    }
}
