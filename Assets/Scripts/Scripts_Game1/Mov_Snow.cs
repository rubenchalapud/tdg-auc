using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov_Snow : MonoBehaviour
{
    // Start is called before the first frame update

    private Rigidbody rbObjetivo;
    public float Velocidad = 5f;
    private Control_Game1 control_Game1;

    public int valorPuntos;
    public int valorTemp;


    void Start()
    {
        rbObjetivo = GetComponent<Rigidbody>();

        transform.position = new Vector3(Random.Range(-25, 18), Random.Range(40,-20), 80);

        control_Game1 = GameObject.Find("GestorJuego").GetComponent<Control_Game1>();
 
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= transform.up * Velocidad * Time.deltaTime;
    }

    private void OnMouseDown() {
        if(control_Game1.juegoEstaActivo)
        {
            Destroy(gameObject);
            control_Game1.ActualizarMarcador(valorPuntos, valorTemp);
            control_Game1.ActualizarPuntos(valorPuntos);
            control_Game1.MoverIndicadorTemperatura(valorTemp);
        }
        
    }

    private void OnTriggerEnter(Collider other) {
        Destroy(gameObject);
    }
}
