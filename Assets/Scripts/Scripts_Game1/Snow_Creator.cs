using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snow_Creator : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Snows;
    public float tiempoCreacion = 2f, RangoCreacion = 2f;

    void Start()
    {
        InvokeRepeating("Creando", 0.0f, tiempoCreacion);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Creando()
    {
        Vector3 SpawnPosition = new Vector3 (0,0,0);
        SpawnPosition = this.transform.position + Random.onUnitSphere * RangoCreacion;
        SpawnPosition = new Vector3 (SpawnPosition.x, SpawnPosition.y, 90);

        GameObject Snow = Instantiate (Snows, SpawnPosition, Quaternion.identity); 

    }
}
