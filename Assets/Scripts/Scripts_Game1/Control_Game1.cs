using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Control_Game1 : MonoBehaviour
{
    public List <GameObject> objetivos;
    private float retrasoGeneracion = 0.5f;
    private int marcador, temperatura, puntos;
    public GameObject imgGameOver;
    public GameObject imgGameWin;
    public TextMeshProUGUI textoTemperatura;
    public TextMeshProUGUI textoTiempo;
    public TextMeshProUGUI textoPuntos, textoPuntosWin;
    public float timeOver = 60f;
    private int timeToText;
    public GameObject indicadorTemp;
    public GameObject indicadorTiempo;
    public GameObject MensajeBox;
    public bool juegoEstaActivo, gameWinActivo;

    private string puntosPrefsName="PuntosGame1";

    // Start is called before the first frame update
    void Start()
    {
        juegoEstaActivo = true;
        gameWinActivo = false;
        StartCoroutine(GenerarObjeetivos());
        marcador = 0;
        puntos = 0;
        temperatura = 246;
        ActualizarMarcador(0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        ContadorTiempo();        
    }

    IEnumerator GenerarObjeetivos()
    {
        while (juegoEstaActivo)
        {
            yield return new WaitForSeconds(retrasoGeneracion);
            int index = Random.Range(0, objetivos.Count);
            Instantiate(objetivos[index]);

        }
        
    }

    public void ContadorTiempo()
    {
        timeOver -= Time.deltaTime;
        timeToText = (int)timeOver;
        textoTiempo.text = "¡" + timeToText.ToString() + " Seg!";
        indicadorTiempo.transform.position = indicadorTiempo.transform.position + new Vector3(9*Time.deltaTime, 0, 0);
        
        if(timeOver <= 0)
        {
            GameOver();
        } 

    }

 
    public void ActualizarMarcador(int puntosASumar, int tempDism){
        marcador += puntosASumar;
        temperatura += tempDism;
        textoTemperatura.text = temperatura + "°C";
        if(temperatura <= 24){
            GameWin();
        }
    }

    public void ActualizarPuntos(int puntosASumar)
    {
        puntos += puntosASumar;
        textoPuntos.text = puntos + "";
        textoPuntosWin.text = puntos + "";
    }

    public void MoverIndicadorTemperatura(int tempDism)
    {
        if(tempDism==3)
        {
            indicadorTemp.transform.position = indicadorTemp.transform.position + new Vector3(0,6,0);
        }
        else
        {
            indicadorTemp.transform.position = indicadorTemp.transform.position + new Vector3(0,-12,0);
        }
        

    }

    public void GameOver()
    {
        if(gameWinActivo == false)
        {
            imgGameOver.gameObject.SetActive(true);
        }
        JuegoFinalizado();
    }

    public void GameWin()
    {
        imgGameWin.gameObject.SetActive(true);
        gameWinActivo = true;
        JuegoFinalizado();
    }

    public void JuegoFinalizado()
    {
        juegoEstaActivo = false;
        textoTiempo.gameObject.SetActive(false);
        indicadorTiempo.gameObject.SetActive(false);
        MensajeBox.gameObject.SetActive(false);
        Invoke("ChangeScene", 5f);
    }

    private void ChangeScene()
    {
        SceneManager.LoadScene("Juego_2");
    }

    private void OnDestroy() {
        SaveData();
    }

    private void SaveData()
    {
        PlayerPrefs.SetInt(puntosPrefsName , puntos);
        //Para cargar los datos en la otra escena:
        //int puntosUno = PlayerPrefs.GetInt("PuntosGame1", 0);
    }
     
}
