using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;

public class CargarDatos : MonoBehaviour
{
    DatabaseReference reference;
    private List<User> rankingJugador = new List<User>();
    public Transform entryContainer, entryTemplate;
    public GameObject txtCargando;
    float templateHeight = 70f;
    private bool cargo;
    public Sprite medal1, medal2, medal3, medalv; 

    // Start is called before the first frame update
    void Start()
    {
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        cargo = false;
        ObtenerRegistros();  
    }

    // Update is called once per frame
    void Update()
    {
      if (cargo == true){
        txtCargando.gameObject.SetActive(false);
        CrearObjeto();
      }
    }

    private void Awake() {
      //Desactivar template
      entryTemplate.gameObject.SetActive(false);
    }

    //Obtener registros de Firebase y agregarlos a Lista de objetos 
    void ObtenerRegistros()
    {
        FirebaseDatabase.DefaultInstance.GetReference("User").GetValueAsync().ContinueWith(task => {
        if (task.IsFaulted) {
          // Handle the error...
          Debug.Log("ErrorLecturaDatos");
        }
        else if (task.IsCompleted) {
          DataSnapshot snapshot = task.Result;
          // Do something with snapshot...
          foreach(var User in snapshot.Children)
          {
              User usuarioRetornado = JsonUtility.FromJson<User>(User.GetRawJsonValue());
              rankingJugador.Add(usuarioRetornado);
          }
          OrdenarRanking();
          cargo = true;
        }
      });
    }

    //Crear objeto por dato de lista para representarlo en la tabla de la scena
    void CrearObjeto()
    {
      for(int i = 0; i < rankingJugador.Count; i++)
      {
        User jg = rankingJugador[i];
        Transform entryTransform = Instantiate(entryTemplate, entryContainer);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, 276-templateHeight * i);
        int rank = i + 1;

        if(i == 0)
        {
          entryTransform.GetComponent<SetScore>().SetScores(medal1, rank, jg.username, jg.puntaje.ToString());
        }
        else if(i == 1)
        {
          entryTransform.GetComponent<SetScore>().SetScores(medal2, rank, jg.username, jg.puntaje.ToString());
        }
        else if(i == 2)
        {
          entryTransform.GetComponent<SetScore>().SetScores(medal3, rank, jg.username, jg.puntaje.ToString());
        }
        else
        {
          entryTransform.GetComponent<SetScore>().SetScores(medalv, rank, jg.username, jg.puntaje.ToString());
        }

        entryTransform.gameObject.SetActive(true);
      }
      cargo = false;
    }

    //Ornenar ranking de mayo puntajea menor de acuerdo a datos en la lista de jugadores
    void OrdenarRanking()
    {
      for(int i = 0; i < rankingJugador.Count; i++)
      {
        for(int j = i + 1; j < rankingJugador.Count; j++)
        {
          if(rankingJugador[j].puntaje > rankingJugador[i].puntaje)
          {
            User tmp = rankingJugador[i];
            rankingJugador[i] = rankingJugador[j];
            rankingJugador[j] = tmp;
          }
        }
      }
    }

}
