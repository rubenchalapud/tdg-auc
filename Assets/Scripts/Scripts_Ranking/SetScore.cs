using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetScore : MonoBehaviour
{
    public GameObject nombre;
    public GameObject puntuacion;
    public GameObject puesto;
    public Image insignia;

    public void SetScores(Sprite spriteJu, int puestoJugador, string nombreJugador, string puntuacionJugador)
    {
        insignia.sprite = spriteJu;
        puesto.GetComponent<Text>().text = puestoJugador.ToString()+".";
        nombre.GetComponent<Text>().text = nombreJugador;
        puntuacion.GetComponent<Text>().text = puntuacionJugador;
    }
}
