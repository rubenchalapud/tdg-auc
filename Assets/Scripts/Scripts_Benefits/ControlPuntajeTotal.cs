using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Firebase;
using Firebase.Database;
using UnityEngine.UI;


public class ControlPuntajeTotal : MonoBehaviour
{
    private int puntosG1, puntosG2, puntosG3, puntosG4;
    public TextMeshProUGUI txtPuntajeTotal;
    [SerializeField] Text textPuntajeShare;
    DatabaseReference reference;
    [SerializeField] InputField username;
    [SerializeField] int puntajeTotal;

    // Start is called before the first frame update
    void Start()
    {
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        CargarPuntosDeMemoria();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void CargarPuntosDeMemoria()
    {
        puntosG1 = PlayerPrefs.GetInt("PuntosGame1", 0);
        puntosG2 = PlayerPrefs.GetInt("PuntosGame2", 0);
        puntosG3 = PlayerPrefs.GetInt("PuntosGame3", 0);
        puntosG4 = PlayerPrefs.GetInt("PuntosGame4", 0);
        MostrarPuntosEnPantalla();
    }

    private void MostrarPuntosEnPantalla()
    {
        puntajeTotal = puntosG1 + puntosG2 + puntosG3 + puntosG4;
        txtPuntajeTotal.text = puntajeTotal + "";
        textPuntajeShare.text = puntajeTotal + "";
    }

    public void SaveData()
    {
        User user = new User();
        user.username = username.text;
        user.puntaje = puntajeTotal;
        string json = JsonUtility.ToJson(user);

        reference.Child("User").Child(user.username).SetRawJsonValueAsync(json).ContinueWith(task => 
        {
            if(task.IsCompleted)
            {
                Debug.Log("Se cargaron los datos en Firebase");
            }else
            {
                Debug.Log("Fallo carga a Firebase");
            }
        });
    }
}
