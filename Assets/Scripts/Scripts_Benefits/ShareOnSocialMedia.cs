using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;


public class ShareOnSocialMedia : MonoBehaviour
{
    [SerializeField] GameObject PanelShare;
    [SerializeField] Text txtDate;


    public void ShareScore(){
        DateTime dt = DateTime.Now;

        txtDate.text = string.Format ("{0}/{1}/{2}", dt.Month, dt.Day, dt.Year);

        PanelShare.SetActive(true);
        StartCoroutine ("TakeScreenShotAndShare");
    }

    IEnumerator TakeScreenShotAndShare ()
	{
		yield return new WaitForEndOfFrame ();

		Texture2D tx = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);
		tx.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0);
		tx.Apply ();

		string path = Path.Combine (Application.temporaryCachePath, "sharedImage.png");//image name
		File.WriteAllBytes (path, tx.EncodeToPNG ());

		Destroy (tx); //to avoid memory leaks

		new NativeShare ()
			.AddFile (path)
			.SetSubject ("This is my score")
			.SetText ("share your score with your friends")
			.Share ();

		PanelShare.SetActive (false); //hide the panel
	}
}
