using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov_Calle : MonoBehaviour
{
    private float velocidad = 10f;
    private Rigidbody rbObjetivoG;
    private ControlGame3 controlGame3;


    // Start is called before the first frame update
    void Start()
    {
        rbObjetivoG = GetComponent<Rigidbody>();
        controlGame3 = GameObject.Find("GestorJuego3").GetComponent<ControlGame3>();
    }

    private void ReposicionarFondo()
    {
        gameObject.transform.position = new Vector3(0, (33f*3f), 12);
    }

    // Update is called once per frame
    void Update()
    {
        if(controlGame3.juegoEstaActivo)
        {
            transform.position -= transform.up * velocidad * Time.deltaTime;

            if(transform.position.y < -33)
            {
                ReposicionarFondo();
            }
        }
    }

}
