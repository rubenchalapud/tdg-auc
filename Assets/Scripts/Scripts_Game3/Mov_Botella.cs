using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov_Botella : MonoBehaviour
{
    private Rigidbody rbObjetivo;
    private float posicionX;

    // Start is called before the first frame update
    void Start()
    {
        rbObjetivo = GetComponent<Rigidbody>();
        transform.position = new Vector3(0, -3.4f, 0);
        posicionX = 0;
    }

    // Update is called once per frame
    void Update()
    {
 
    }

    public void MovBtnIzq()
    {
        if(posicionX == 0)
        {
            posicionX = -1;
            transform.position = new Vector3(-1.35f, -3.4f, 0);
        }
        if(posicionX == 1)
        {
            posicionX = 0;
            transform.position = new Vector3(0, -3.4f, 0);
        }
    }

    public void MovBtnDer()
    {
        if(posicionX == 0)
        {
            posicionX = 1;
            transform.position = new Vector3(1.35f, -3.4f, 0);
        }
        if(posicionX == -1)
        {
            posicionX = 0;
            transform.position = new Vector3(0, -3.4f, 0);
        }
    }
}
