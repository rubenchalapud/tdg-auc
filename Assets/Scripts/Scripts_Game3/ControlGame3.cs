using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ControlGame3 : MonoBehaviour
{
    public List <GameObject> objetivos;
    private float retrasoGeneracion = 0.6f;
    private float litros;
    public GameObject indicadorTiempo, mensajeInicio, marcadorCantidadA, marcadorAceite, gameOverBox, gameOverWinBox;
    public float timeOver = 60f;
    private int timeToText, puntos;
    public bool juegoEstaActivo, gameOverActivo;
    public TextMeshProUGUI textoTiempo, textoMililitros, textoPuntos, textoPuntosWin;
    private string puntosPrefsName="PuntosGame3";
    // Start is called before the first frame update
    void Start()
    {
        juegoEstaActivo = true;
        gameOverActivo = false;
        litros = 1;
        puntos = 0;
        StartCoroutine(GenerarObjeetivos());
        
    }

    // Update is called once per frame
    void Update()
    {
        ContadorTiempo();
        
    }

    IEnumerator GenerarObjeetivos()
    {
        while (juegoEstaActivo)
        {
            yield return new WaitForSeconds(retrasoGeneracion);
            int index = Random.Range(0, objetivos.Count);
            Instantiate(objetivos[index]);

        }   
    }

    public void ContadorTiempo()
    {
        timeOver -= Time.deltaTime;
        timeToText = (int)timeOver;
        textoTiempo.text = "¡" + timeToText.ToString() + " M!";
        indicadorTiempo.transform.position = indicadorTiempo.transform.position + new Vector3(9*Time.deltaTime, 0, 0);
        
        if(timeOver <= 0)
        {
            GameWin();
        } 
    }

    public void ActualizarPuntos(int puntosASumar)
    {
        puntos += puntosASumar;
        textoPuntos.text = puntos + "";
        textoPuntosWin.text = puntos + "";
    }

    public void BajarAceiteYMarcador(float valorBajada)
    {
        if(juegoEstaActivo)
        {
            marcadorCantidadA.transform.position = marcadorCantidadA.transform.position + new Vector3(0, valorBajada * 80, 0);
            marcadorAceite.transform.position = marcadorAceite.transform.position + new Vector3(0, valorBajada * 40, 0);
            marcadorAceite.transform.localScale = marcadorAceite.transform.localScale + new Vector3 (0, valorBajada * 0.8f, 0); 
        }
       
    }

    public void ActualizarValorMililitros(float valorMililitros)
    {
        if(juegoEstaActivo)
        {
            litros += valorMililitros;
            textoMililitros.text = litros.ToString("F2") + " L";
            if(litros<=0)
            {
                GameOver();
            }
        }
    }

    public void GameOver() 
    {
        gameOverBox.gameObject.SetActive(true);
        gameOverActivo = true;
        JuegoFinalizado();
    }

    public void GameWin()
    {
        if(gameOverActivo == false)
        {
            gameOverWinBox.gameObject.SetActive(true);
        }
        JuegoFinalizado();
    }
    
    public void JuegoFinalizado()
    {
        juegoEstaActivo = false;
        mensajeInicio.gameObject.SetActive(false);
        textoTiempo.gameObject.SetActive(false);
        indicadorTiempo.gameObject.SetActive(false);
        Invoke("ChangeScene", 5f);
    }
    private void ChangeScene()
    {
        SceneManager.LoadScene("Juego_4");
    }

    private void OnDestroy() {
        SaveData();
    }

    private void SaveData()
    {
        PlayerPrefs.SetInt(puntosPrefsName , puntos);
        //Para cargar los datos en la otra escena:
        //int puntosUno = PlayerPrefs.GetInt("PuntosGame1", 0);
    }
}
