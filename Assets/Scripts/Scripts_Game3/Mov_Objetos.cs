using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov_Objetos : MonoBehaviour
{
    private float velocidad = 4.6f;
    private Rigidbody rbObjetivoG;
    private ControlGame3 controlGame3;
    public float valorMililitrosPerdidos;
    public float valorBajada;
    public int puntos;
    // Start is called before the first frame update
    void Start()
    {
        rbObjetivoG = GetComponent<Rigidbody>();
        controlGame3 = GameObject.Find("GestorJuego3").GetComponent<ControlGame3>();
        int r = Random.Range(1,4);
        if(r==1)
        {
            transform.position = new Vector3(-1.25f, 7.6f, 0);
        }else if(r==2)
        {
            transform.position = new Vector3(0, 7.6f, 0);
        }
        else
        {
            transform.position = new Vector3(1.25f, 7.6f, 0);
        }

    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= transform.up * velocidad * Time.deltaTime;
        
    }

    private void OnTriggerEnter(Collider other) 
    {
        if(other.tag == "sensor")
        {
            Destroy(gameObject);
        }
        else
        {
            controlGame3.ActualizarPuntos(puntos);
            controlGame3.BajarAceiteYMarcador(valorBajada);
            controlGame3.ActualizarValorMililitros(valorMililitrosPerdidos);
            Destroy(gameObject);
        }
        
    }


}
