 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov_Bot : MonoBehaviour
{
    public Camera cam;

    public void MoverBotella()
    {
        cam = Camera.main;
        Vector3 v = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10);
        Vector3 mousePosX = cam.ScreenToWorldPoint(v);      
        //Debug.Log(mousePosX + "");
        this.transform.position = new Vector3(mousePosX.x, -4.5f, -0.45f);
    }
}
