using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Control_Game2 : MonoBehaviour
{
    public List <GameObject> objetivosG;
    private float retraGeneracion = 0.2f;
    private int puntos;
    private float litros;
    public GameObject aceiteBotella, marcadorCantidadA, marcadorAceite, gameOverBox, gameOverWinBox, mensajeInicio;
    public GameObject indicadorTiempo, sarten;
    public TextMeshProUGUI textoTiempo, textoMililitros, textoPuntos, textoPuntosWin;
    public float timeOver = 60f;
    private int timeToText;
    public bool juegoEstaActivo, gameWinActivo;

    private string puntosPrefsName="PuntosGame2";
    // Start is called before the first frame update
    void Start()
    {
        juegoEstaActivo = true;
        gameWinActivo = false;
        litros = 0;
        puntos = 0;
        StartCoroutine(GenerarGotas());   
    }

    // Update is called once per frame
    void Update()
    {
        ContadorTiempo();
    
    }

    IEnumerator GenerarGotas()
    {
        while(juegoEstaActivo)
        {
            yield return new WaitForSeconds(retraGeneracion);
            int index = Random.Range(0, objetivosG.Count);
            Instantiate(objetivosG[index]);
        }
    }

    public void MoverSarten(float posicionX)
    {
        sarten.transform.position = new Vector3(posicionX, 3.35f, 0);
    }

    public void ContadorTiempo()
    {
        timeOver -= Time.deltaTime;
        timeToText = (int)timeOver;
        textoTiempo.text = "¡" + timeToText.ToString() + " Seg!";
        indicadorTiempo.transform.position = indicadorTiempo.transform.position + new Vector3(9*Time.deltaTime, 0, 0);
        
        if(timeOver <= 0)
        {
            GameOver();
        } 
    }

    public void ActualizarPuntos(int puntosASumar)
    {
        puntos += puntosASumar;
        textoPuntos.text = puntos + "";
        textoPuntosWin.text = puntos + "";
    }

    public void SubirAceiteYMarcador(float valorSubida)
    {
        if(juegoEstaActivo)
        {
            aceiteBotella.transform.position = aceiteBotella.transform.position + new Vector3(0, valorSubida, 0);
            marcadorCantidadA.transform.position = marcadorCantidadA.transform.position + new Vector3(0, valorSubida * 80, 0);
            marcadorAceite.transform.position = marcadorAceite.transform.position + new Vector3(0, valorSubida * 40, 0);
            marcadorAceite.transform.localScale = marcadorAceite.transform.localScale + new Vector3 (0, valorSubida * 0.8f, 0); 
        }
       
    }

    public void ActualizarValorMililitros(float valorMililitros)
    {
        if(juegoEstaActivo)
        {
            litros += valorMililitros;
            textoMililitros.text = litros.ToString("F2") + " L";
            if(litros>=1)
            {
                GameWin();
            }
        }
    }

    public void GameOver() 
    {
        if(gameWinActivo == false)
        {
            gameOverBox.gameObject.SetActive(true);
        }
        JuegoFinalizado();
    }

    public void GameWin()
    {
        gameOverWinBox.gameObject.SetActive(true);
        gameWinActivo = true;
        JuegoFinalizado();
    }
    
    public void JuegoFinalizado()
    {
        juegoEstaActivo = false;
        mensajeInicio.gameObject.SetActive(false);
        textoTiempo.gameObject.SetActive(false);
        indicadorTiempo.gameObject.SetActive(false);
        Invoke("ChangeScene", 5f);
    }
    private void ChangeScene()
    {
        SceneManager.LoadScene("Juego_3");
    }

    private void OnDestroy() {
        SaveData();
    }

    private void SaveData()
    {
        PlayerPrefs.SetInt(puntosPrefsName , puntos);
        //Para cargar los datos en la otra escena:
        //int puntosUno = PlayerPrefs.GetInt("PuntosGame1", 0);
    }
     
}
