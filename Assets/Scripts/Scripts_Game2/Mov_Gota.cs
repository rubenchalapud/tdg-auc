using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov_Gota : MonoBehaviour
{
    private Rigidbody rbObjetivoG;
    public float velocidad = 5f;
    private Control_Game2 control_game2;
    public int valorPuntos;
    public float valorMililitros;
    public float valorSubida;
    private float PosicionX;

    // Start is called before the first frame update
    void Start()
    {
        rbObjetivoG = GetComponent<Rigidbody>();
        transform.position = new Vector3(Random.Range(-2.50f, 2.50f), 0, 0);
        control_game2 = GameObject.Find("GestorJuego2").GetComponent<Control_Game2>();
        ObtenerPosicionX();      
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= transform.up * velocidad * Time.deltaTime;
    }

    public void ObtenerPosicionX()
    {
        PosicionX = transform.position.x;
        control_game2.MoverSarten(PosicionX);
        //Debug.Log(PosicionX);

    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag =="BotellaTrigger")
        {
            //Puntos
            control_game2.ActualizarPuntos(valorPuntos);
            control_game2.SubirAceiteYMarcador(valorSubida);
            control_game2.ActualizarValorMililitros(valorMililitros);
            //Debug.Log("EntroAceite");
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        
    }
}
