using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;  
using TMPro;

public class DialogueAndScenes : MonoBehaviour
{

    public TextMeshProUGUI DialogueText;
    public string[] Sentences;
    private int Index = 0;
    public float DialogueSpeed;
    public GameObject DialogCanvas;
    private bool nextText = true;

    public void StartDialog()
    {
    
     if(nextText == true){ 
            NextSentence(); 
            nextText = false;
       
      }
      
    }

    void NextSentence()
    {
        if(Index <= Sentences.Length - 1)
        {
            DialogueText.text = "";
            StartCoroutine(WriteSentence());
            StartCoroutine(ChangeScene());
        }

    }


    IEnumerator WriteSentence()
    {
        foreach(char Character in Sentences[Index].ToCharArray())
            {
                DialogueText.text += Character;
                yield return new WaitForSeconds(DialogueSpeed);   
                
            }
            Index++;
            nextText = true;
    }

    IEnumerator ChangeScene(){
        yield return new WaitForSeconds(15);
        SceneManager.LoadScene("Juego_1"); 
    }

    
}

